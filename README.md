# Gitlab CI Templates

## `debian-package.yaml`: CI for Debian packages

A template to build, lint and deploy debian packages. The deploy step is
only run on tagged commits on master.

### Usage

```yaml
include:
    - https://gitlab.gnugen.ch/gnugen/gitlab-ci-template/-/raw/master/debian-package.yaml

variables:
    PKGNAME: gnugen-members-management
```
More complex examples:

* https://gitlab.gnugen.ch/gnugen/members-management/-/tree/master/.gitlab-ci.yml
* https://gitlab.gnugen.ch/gnugen/riot-integration/-/tree/master/.gitlab-ci.yml
* https://gitlab.gnugen.ch/gnugen/gnupaste/-/tree/master/.gitlab-ci.yml

### Use a custom build before packaging

Sometimes you need to run some commands before building the debian package,
because the package isn't a proper package yet. In such cases you can add

```yaml
debpkg/package:
  before_script:
    - stack build
```

Don't override `script:` though, or the deb won't be built.

### Disabling Lintian

If need be, you can disable lintian.

```yaml
variables:
    DISABLE_LINTIAN: true
```



## Various

MIT license. Inspired by https://salsa.debian.org/salsa-ci-team/pipeline/
